import React from 'react';
import {Progress} from 'react-sweet-progress';
import "react-sweet-progress/lib/style.css";

import './sass/CustomProgressBar.sass';

const CustomProgressBar = ({percent, nameSkills}) => {

    const getStatus = () => {
        if (percent > 80) {
            return 'success'
        } else if (percent > 49 && percent <= 80) {
            return 'middle'
        } else {
            return 'error';
        }
    };

    return (
        <div className="custom-progress-bar">
            <div className="title">
                <span>{nameSkills.toUpperCase()}</span>
                <span className={getStatus()}>{percent}</span>
            </div>
            <Progress
                percent={percent}
                status={getStatus()}
                theme={{
                    success: {
                        symbol: ' ',
                        color: '#007D3C'
                    },
                    middle: {
                        symbol: ' ',
                        color: '#FF862C'
                    },
                    error: {
                        symbol: ' ',
                        color: '#D40017'
                    }
                }}
            />
        </div>
    );
};

export default CustomProgressBar;
