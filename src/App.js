import React, {Component} from 'react';

import './styles/infographic.sass';
import Player from "./containers/Player/Player";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            playerId: 1
        }
    }

    handleChosenPlayer(playerId) {
        this.setState({
            playerId
        })
    }

    render() {
        return (
            <div className="infographic">
                <div className="title">
                    <h1>Top <span>10</span> players in <span id="fifa-logo">FIFA 19</span></h1>
                </div>
                <article>
                    <div className="body">
                        <div id="kross" className="box right">
                            <div className="player-name">
                                <div className="number-line">
                                    <div className="number">
                                        <p>10</p>
                                        <p>TONI KROOS</p>
                                    </div>
                                    <div className="line">
                                        <span/>
                                        <span/>
                                    </div>
                                </div>
                                <div className="desc">
                                    <p>
                                        Błyskotliwy motor napędzający potężną machinę Realu Madryt.
                                    </p>
                                </div>
                            </div>
                            <div className="square">
                                <img src="/images/kroos.png" alt="" onClick={() => this.handleChosenPlayer(10)}/>
                            </div>
                        </div>
                        <div id="gea" className="box">
                            <div className="player-name">
                                <div className="number-line">
                                    <div className="number">
                                        <p>09</p>
                                        <p>DAVID DE GEA</p>
                                    </div>
                                    <div className="line">
                                        <span/>
                                        <span/>
                                    </div>
                                </div>
                                <div className="desc">
                                    <p>
                                        Najwyżej oceniony bramkarz świata ma nerwy ze stali oraz refleks i parady na
                                        poziomie 94 i 90.
                                    </p>
                                </div>
                            </div>
                            <div className="square">
                                <img src="/images/gea.png" alt="" onClick={() => this.handleChosenPlayer(9)}/>
                            </div>
                        </div>
                        <div id="suarez" className="box right">
                            <div className="player-name">
                                <div className="number-line">
                                    <div className="number">
                                        <p>08</p>
                                        <p>LUIS SUÁREZ</p>
                                    </div>
                                    <div className="line">
                                        <span/>
                                        <span/>
                                    </div>
                                </div>
                                <div className="desc">
                                    <p>
                                        Bezlitosny środkowy napastnik Barcelony nieustannie frustruje rywali swoją
                                        nieuchwytnością i siłą.
                                    </p>
                                </div>
                            </div>
                            <div className="square">
                                <img src="/images/suarez.png" alt="" onClick={() => this.handleChosenPlayer(8)}/>
                            </div>
                        </div>
                        <div id="ramos" className="box">
                            <div className="player-name">
                                <div className="number-line">
                                    <div className="number">
                                        <p>07</p>
                                        <p>SERGIO RAMOS</p>
                                    </div>
                                    <div className="line">
                                        <span/>
                                        <span/>
                                    </div>
                                </div>
                                <div className="desc">
                                    <p>
                                        Ramos jest najlepszym środkowym obrońcą świata. 83 punkty warunków fizycznych
                                        robią wrażenie.
                                    </p>
                                </div>
                            </div>
                            <div className="square">
                                <img src="/images/ramos.png" alt="" onClick={() => this.handleChosenPlayer(7)}/>
                            </div>
                        </div>
                        <div id="hazard" className="box right">
                            <div className="player-name">
                                <div className="number-line">
                                    <div className="number">
                                        <p>06</p>
                                        <p>EDEN HAZARD</p>
                                    </div>
                                    <div className="line">
                                        <span/>
                                        <span/>
                                    </div>
                                </div>
                                <div className="desc">
                                    <p>
                                        Hazard to kolejny wszechstronny zawodnik o ofensywnych inklinacjach, który
                                        skrzydłowym jest tylko z nazwy, bo potrafi demolować rywali.
                                    </p>
                                </div>
                            </div>
                            <div className="square">
                                <img src="/images/hazard.png" alt="" onClick={() => this.handleChosenPlayer(6)}/>
                            </div>
                        </div>
                        <div id="bruyne" className="box">
                            <div className="player-name">
                                <div className="number-line">
                                    <div className="number">
                                        <p>05</p>
                                        <p>KEVIN DE BRUYNE</p>
                                    </div>
                                    <div className="line">
                                        <span/>
                                        <span/>
                                    </div>
                                </div>
                                <div className="desc">
                                    <p>
                                        De Bruyne to kolejny Belg podbijający futbolowe salony. Rozdaje karty w
                                        Manchesterze City.
                                    </p>
                                </div>
                            </div>
                            <div className="square">
                                <img src="/images/bruyne.png" alt="" onClick={() => this.handleChosenPlayer(5)}/>
                            </div>
                        </div>
                        <div id="modirc" className="box right">
                            <div className="player-name">
                                <div className="number-line">
                                    <div className="number">
                                        <p>04</p>
                                        <p>LUKA MODRIĆ</p>
                                    </div>
                                    <div className="line">
                                        <span/>
                                        <span/>
                                    </div>
                                </div>
                                <div className="desc">
                                    <p>
                                        Zdobywca Złotej Piłki ostatnich mistrzostw świata od wielu lat utrzymuje wysoką
                                        formę.
                                    </p>
                                </div>
                            </div>
                            <div className="square">
                                <img src="/images/modric.png" alt="" onClick={() => this.handleChosenPlayer(4)}/>
                            </div>
                        </div>
                        <div id="neymar" className="box">
                            <div className="player-name">
                                <div className="number-line">
                                    <div className="number">
                                        <p>03</p>
                                        <p>NEYMAR</p>
                                    </div>
                                    <div className="line">
                                        <span/>
                                        <span/>
                                    </div>
                                </div>
                                <div className="desc">
                                    <p>
                                        Mistrz samby zyskał jeszcze większą sławę po transferze do Paris Saint-Germain.
                                    </p>
                                </div>
                            </div>
                            <div className="square">
                                <img src="/images/neymar.png" alt="" onClick={() => this.handleChosenPlayer(3)}/>
                            </div>
                        </div>
                        <div id="messi" className="box right">
                            <div className="player-name">
                                <div className="number-line">
                                    <div className="number">
                                        <p>02</p>
                                        <p>LIONEL MESSI</p>
                                    </div>
                                    <div className="line">
                                        <span/>
                                        <span/>
                                    </div>
                                </div>
                                <div className="desc">
                                    <p>
                                        Argentyński napastnik po latach bicia rekordów i dziesiątkach zdobytych trofeów
                                        wciąż utrzymuje się na szczycie.
                                    </p>
                                </div>
                            </div>
                            <div className="square">
                                <img src="/images/messi.png" alt="" onClick={() => this.handleChosenPlayer(2)}/>
                            </div>
                        </div>
                        <div id="ronaldo" className="box">
                            <div className="player-name">
                                <div className="number-line">
                                    <div className="number">
                                        <p>01</p>
                                        <p>CRISTIANO RONALDO</p>
                                    </div>
                                    <div className="line">
                                        <span/>
                                        <span/>
                                    </div>
                                </div>
                                <div className="desc">
                                    <p>
                                        Gwiazda okładki FIFA 19 zebrała w swojej karierze niezliczone nagrody i trofea.
                                    </p>
                                </div>
                            </div>
                            <div className="square">
                                <img src="/images/ronaldo.png" alt="" onClick={() => this.handleChosenPlayer(1)}/>
                            </div>
                        </div>
                    </div>
                    <Player playerId={this.state.playerId}/>
                </article>
            </div>
        );
    }
}

export default App;
