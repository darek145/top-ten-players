import React, {Component} from 'react';
import players from '../../data/players';

import './sass/player.sass'
import CustomProgressBar from "../../components/CustomProgressBar/CustomProgressBar";

class Player extends Component {
    constructor(props) {
        super(props);

        this.state = {
            player: this.getPlayer(props.playerId)
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps !== this.props) {
            const player = this.getPlayer(nextProps.playerId);
            this.setState({
                player
            })
        }
    }

    getPlayer(playerId) {
        return players.top.find(player => player.id === playerId);
    }

    renderProgressBars() {
        const {player} = this.state;
        return Object.entries(player.skills).map(([key, value]) => {
            return <CustomProgressBar percent={value} nameSkills={key}/>
        });
    }

    render() {
        const {player} = this.state;

        return (
            <div className="currency-player">
                <div className="header">
                    <h2>{player.name}</h2>
                    <img src={`/images/${player.card_img}`} alt="card"/>
                </div>
                <div className="general">
                    <div className="row">
                        <div className="cell">
                            <div>CLUB</div>
                            <div>{player.general.club}</div>
                        </div>
                        <div className="cell">
                            <div>POSITION</div>
                            <div>{player.general.position}</div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="cell">
                            <div>LEAGUE</div>
                            <div>{player.general.league}</div>
                        </div>
                        <div className="cell">
                            <div>AGE</div>
                            <div>{player.general.age}</div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="cell">
                            <div>HEIGHT</div>
                            <div>{player.general.height}</div>
                        </div>
                        <div className="cell">
                            <div>WEIGHT</div>
                            <div>{player.general.weight}</div>
                        </div>
                    </div>
                </div>
                <div className="skills">
                    {this.renderProgressBars()}
                </div>
            </div>
        );
    }
}

Player.propTypes = {};

export default Player;
